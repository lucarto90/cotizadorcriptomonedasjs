class Interfaz {
    constructor () {
        this.init();
    }

    init () {
        this.construirSelect();
    }

    mostrarMensaje(mensaje, clases) {
        const div = document.createElement('div');
        div.className = clases;
        div.appendChild(document.createTextNode(mensaje));

        // seleccinar mensajes
        const divMensaje = document.querySelector('.mensajes');
        divMensaje.appendChild(div);

        // mostrar contenidos
        setTimeout(() => {
            document.querySelector('.mensajes').remove();
        },3000);
    }

    construirSelect () {
        cotizador.obtenerMonedasAPI()
            .then(monedas => {
                // crear un select de opciones
                const select = document.querySelector('#criptomoneda');

                // iterar por las opciones del api
                for ( const [key, value] of Object.entries(monedas.monedas.Data) ) {
                    // añadir el symbol y el nombre como opciones
                    const opcion = document.createElement('option');
                    opcion.value = value.Symbol;
                    opcion.appendChild(document.createTextNode(value.CoinName));
                    select.appendChild(opcion);
                }
            });
    }

    // imprimir el resultado de la cotizacion
    mostrarResultado(resultado, moneda, cripto) {
        // en caso de un resultado anterior ocultarlo
        const resultadoAnterior = document.querySelector('#resultado > div');

        if (resultadoAnterior) {
            resultadoAnterior.remove();
        }

        const datosMoneda = resultado[cripto][moneda];

        // recortar digitos de precio
        let precio = new Intl.NumberFormat().format(datosMoneda.PRICE),
            porcentaje = datosMoneda.CHANGEPCTDAY.toFixed(2),
            actualizado = new Date(datosMoneda.LASTUPDATE * 1000).toLocaleDateString('es-CO');

        // construir el template
        let templateHtml = `
            <div class="card bg-warning">
                <div class="card-body text-light">
                    <h2 class="card-title">Resultado:</h2>
                    <p>El precio de ${datosMoneda.FROMSYMBOL} a moneda ${datosMoneda.TOSYMBOL} es de: $ ${precio}</p>
                    <p>Variacion ultimo dia: % ${porcentaje}</p>
                    <p>Ultima actualizacion: ${actualizado}</p>
                </div>
            </div>
        `;

        this.mostrarOcultarSpinner('block');

        setTimeout(() => {
            // insertar el resultado
            document.querySelector('#resultado').innerHTML = templateHtml;
            //ocultar el spinner
            this.mostrarOcultarSpinner('none')
        }, 3000);
    }

    // mostrar un spinner de carga al enviar la cotizacion
    mostrarOcultarSpinner (vista) {
        const spinner = document.querySelector('.contenido-spinner');
        spinner.style.display = vista;
    }
}