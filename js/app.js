const cotizador = new API('5a90c0287f03f4a108f7773c751bcf169d783d9c01714ba563f5e97c882d3de9');
const ui = new Interfaz();


// leer el formulario
const formulario = document.querySelector('#formulario');
// eventListener
formulario.addEventListener('submit', (e) => {
    e.preventDefault();

    // leer la moneda seleccionada
    const monedaSelect = document.querySelector('#moneda');
    const monedaSeleccionada = monedaSelect.options[monedaSelect.selectedIndex].value;

    // leer la criptomoneda
    const criptoSelect = document.querySelector('#criptomoneda');
    const criptoSeleccionada = criptoSelect.options[criptoSelect.selectedIndex].value;

    // comprobar que ambos campos tengan algo seleccionado
    if ( monedaSeleccionada === '' || criptoSeleccionada === '') {
        // arrojar una alerta de error
        ui.mostrarMensaje('Ambos campos son obligatorios!!', 'alert bg-danger text-center');
    } else {
        // cosultar la api
        cotizador.obtenerValores(monedaSeleccionada, criptoSeleccionada)
            .then(data => {
                ui.mostrarResultado(data.resultado.RAW, monedaSeleccionada, criptoSeleccionada);
            });
    }
})